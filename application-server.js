import express from 'express';
const server = express();
const port = 3000;

server.get('/', (req, res) => {
  res.send('Hello from the backend!');
});

server.listen(port, () => {
  console.log(`Backend listening at http://localhost:${port}`);
});
